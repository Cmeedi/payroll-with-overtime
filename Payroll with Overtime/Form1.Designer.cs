﻿namespace Payroll_with_Overtime
{
    partial class PayrollWithOvertimeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HourlyPayRateTextBox = new System.Windows.Forms.TextBox();
            this.HoursWorkedTextBox = new System.Windows.Forms.TextBox();
            this.HoursWorkedLabel = new System.Windows.Forms.Label();
            this.HourlyPayRateLabel = new System.Windows.Forms.Label();
            this.GrossPayResultLabel = new System.Windows.Forms.Label();
            this.GrossPayLabel = new System.Windows.Forms.Label();
            this.CalculateButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HourlyPayRateTextBox
            // 
            this.HourlyPayRateTextBox.Location = new System.Drawing.Point(146, 40);
            this.HourlyPayRateTextBox.Name = "HourlyPayRateTextBox";
            this.HourlyPayRateTextBox.Size = new System.Drawing.Size(100, 22);
            this.HourlyPayRateTextBox.TabIndex = 0;
            // 
            // HoursWorkedTextBox
            // 
            this.HoursWorkedTextBox.Location = new System.Drawing.Point(146, 12);
            this.HoursWorkedTextBox.Name = "HoursWorkedTextBox";
            this.HoursWorkedTextBox.Size = new System.Drawing.Size(100, 22);
            this.HoursWorkedTextBox.TabIndex = 1;
            // 
            // HoursWorkedLabel
            // 
            this.HoursWorkedLabel.AutoSize = true;
            this.HoursWorkedLabel.Location = new System.Drawing.Point(37, 15);
            this.HoursWorkedLabel.Name = "HoursWorkedLabel";
            this.HoursWorkedLabel.Size = new System.Drawing.Size(103, 17);
            this.HoursWorkedLabel.TabIndex = 2;
            this.HoursWorkedLabel.Text = "Hours Worked:";
            // 
            // HourlyPayRateLabel
            // 
            this.HourlyPayRateLabel.AutoSize = true;
            this.HourlyPayRateLabel.Location = new System.Drawing.Point(25, 43);
            this.HourlyPayRateLabel.Name = "HourlyPayRateLabel";
            this.HourlyPayRateLabel.Size = new System.Drawing.Size(115, 17);
            this.HourlyPayRateLabel.TabIndex = 3;
            this.HourlyPayRateLabel.Text = "Hourly Pay Rate:";
            // 
            // GrossPayResultLabel
            // 
            this.GrossPayResultLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GrossPayResultLabel.Location = new System.Drawing.Point(146, 79);
            this.GrossPayResultLabel.Name = "GrossPayResultLabel";
            this.GrossPayResultLabel.Size = new System.Drawing.Size(100, 22);
            this.GrossPayResultLabel.TabIndex = 4;
            // 
            // GrossPayLabel
            // 
            this.GrossPayLabel.AutoSize = true;
            this.GrossPayLabel.Location = new System.Drawing.Point(59, 80);
            this.GrossPayLabel.Name = "GrossPayLabel";
            this.GrossPayLabel.Size = new System.Drawing.Size(78, 17);
            this.GrossPayLabel.TabIndex = 5;
            this.GrossPayLabel.Text = "Gross Pay:";
            // 
            // CalculateButton
            // 
            this.CalculateButton.Location = new System.Drawing.Point(13, 127);
            this.CalculateButton.Name = "CalculateButton";
            this.CalculateButton.Size = new System.Drawing.Size(90, 48);
            this.CalculateButton.TabIndex = 6;
            this.CalculateButton.Text = "Calculate Gross Pay";
            this.CalculateButton.UseVisualStyleBackColor = true;
            this.CalculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(109, 127);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(78, 48);
            this.ClearButton.TabIndex = 7;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(204, 127);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(78, 48);
            this.ExitButton.TabIndex = 8;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // PayrollWithOvertimeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 187);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.CalculateButton);
            this.Controls.Add(this.GrossPayLabel);
            this.Controls.Add(this.GrossPayResultLabel);
            this.Controls.Add(this.HourlyPayRateLabel);
            this.Controls.Add(this.HoursWorkedLabel);
            this.Controls.Add(this.HoursWorkedTextBox);
            this.Controls.Add(this.HourlyPayRateTextBox);
            this.Name = "PayrollWithOvertimeForm";
            this.Text = "Payroll With Overtime";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox HourlyPayRateTextBox;
        private System.Windows.Forms.TextBox HoursWorkedTextBox;
        private System.Windows.Forms.Label HoursWorkedLabel;
        private System.Windows.Forms.Label HourlyPayRateLabel;
        private System.Windows.Forms.Label GrossPayResultLabel;
        private System.Windows.Forms.Label GrossPayLabel;
        private System.Windows.Forms.Button CalculateButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ExitButton;
    }
}

